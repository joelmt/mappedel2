package ntnu.idatt2001.joelmt;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PatientRegisterTest {
    PatientRegister register = new PatientRegister();
    Patient patient = new Patient(13010378195L, "John", "Smith", "Cancer", "James Brotherhood");

    /**
     * Method that tests if the method
     * for adding a patient to the register
     * works as intended when the patient does
     * not already exists in the register
     *
     */
    @Test
    void addNewPatient() {
        assertTrue(register.addPatient(patient));
        assertTrue(register.getPatientRegister().contains(patient));
    }

    /**
     * Method that tests if the method
     * for adding a patient to the register
     * works as intended when the patient
     * already exists in the register
     *
     */
    @Test
    void addAlreadyExistingPatient() {
        register.addPatient(patient);
        assertFalse(register.addPatient(patient));
    }

    /**
     * Method that tests if the method
     * for removing a patient from the register
     * works as intended when the patient
     * exists in the register
     *
     */
    @Test
    void removeExistingPatient() {
        register.addPatient(patient);
        assertTrue(register.removePatient(patient));
        assertFalse(register.getPatientRegister().contains(patient));
    }

    /**
     * Method that tests if the method
     * for removing a patient from the register
     * works as intended when the patient does
     * not exists in the register
     *
     */
    @Test
    void removeNotExistingPatient() {
        assertFalse(register.removePatient(patient));
    }
}