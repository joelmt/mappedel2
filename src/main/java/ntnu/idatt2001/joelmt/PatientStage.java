package ntnu.idatt2001.joelmt;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class representing the stage for adding
 * and editing the data of a patient
 */
public class PatientStage extends Stage {
    public enum Mode {
        NEW, VIEW
    }

    private final Mode mode;
    private Patient existingPatient = null;
    private Patient result;
    private TextField firstNameField = (TextField) GUIElementsFactory.getElement("TEXTFIELD");
    private TextField lastNameField = (TextField) GUIElementsFactory.getElement("TEXTFIELD");
    private TextField socialSecurityNumberField = (TextField) GUIElementsFactory.getElement("TEXTFIELD");

    /**
     * Constructor for the PatientStage without a parameter,
     * therefore representing adding a new patient
     */
    public PatientStage() {
        super();
        this.mode = Mode.NEW;
        createStage();
    }

    /**
     * Constructor for the PatientStage with a parameter,
     * therefore representing viewing and editing a patient
     *
     * @param patient patient that is to be viewed and edited
     */
    public PatientStage(Patient patient) {
        super();
        this.mode = Mode.VIEW;
        this.existingPatient = patient;
        createStage();
    }

    /**
     * Getter for the result
     *
     * @return a Patient object
     */
    public Patient getResult() {
        return result;
    }

    /**
     * Method responsible for creating and setting up the
     * stage
     */
    private void createStage() {
        //Setting up all the text fields
        firstNameField.setPromptText("First name");
        lastNameField.setPromptText("Last name");
        socialSecurityNumberField.setPromptText("Social Security Number");
        firstNameField.setPrefWidth(70);
        lastNameField.setPrefWidth(70);
        socialSecurityNumberField.setPrefWidth(110);

        //Creating and setting up the GridPane where all the elements are placed
        GridPane gpTaskPane = (GridPane) GUIElementsFactory.getElement("GRIDPANE");
        gpTaskPane.setHgap(13);
        gpTaskPane.setVgap(6);
        gpTaskPane.setPadding(new Insets(10, 10, 10, 10));

        //Creating and setting up the "ok" and "cancel" buttons
        Button okButton = (Button) GUIElementsFactory.getElement("BUTTON");
        okButton.setText("OK");
        Button cancelButton = (Button) GUIElementsFactory.getElement("BUTTON");
        cancelButton.setText("Cancel");
        gpTaskPane.add(okButton, 1, 4);
        gpTaskPane.add(cancelButton, 2, 4);
        GridPane.setHalignment(okButton, HPos.RIGHT);
        GridPane.setHalignment(cancelButton, HPos.LEFT);

        //Checking if the mode is for viewing a patient, and then filling the text fields with the data
        if (mode == Mode.VIEW) {
            firstNameField.setText(existingPatient.getFirstName());
            lastNameField.setText(existingPatient.getLastName());
            socialSecurityNumberField.setText(String.valueOf(existingPatient.getSocialSecurityNumber()));
        }

        //Creating and setting up all the labels for the text fields
        Label firstNameLabel = (Label) GUIElementsFactory.getElement("LABEL");
        firstNameLabel.setText("First Name");
        gpTaskPane.add(firstNameLabel,1,1);
        gpTaskPane.add(firstNameField, 2, 1, 9, 1);
        Label lastNameLabel = (Label) GUIElementsFactory.getElement("LABEL");
        lastNameLabel.setText("Last Name");
        gpTaskPane.add(new Label("Last name:"), 1, 2);
        gpTaskPane.add(lastNameField, 2, 2, 9, 1);
        Label socialSecurityNumberLabel = (Label) GUIElementsFactory.getElement("LABEL");
        socialSecurityNumberLabel.setText("Social Security Number");
        gpTaskPane.add(socialSecurityNumberLabel, 1, 3);
        gpTaskPane.add(socialSecurityNumberField, 2, 3, 9, 1);

        //Setting the title of the stage depending of what the mode is
        if(mode == Mode.NEW){
            super.setTitle("Add");
        } else if(mode == Mode.VIEW){
            super.setTitle("Edit");
        }

        Scene taskScene = new Scene(gpTaskPane, 460, 300);
        super.setScene(taskScene);
        super.initModality(Modality.APPLICATION_MODAL);


        okButton.setOnAction(actionEvent -> {
            //Setting the result or updating the existing patients info depending of what the mode
            if (mode == Mode.NEW) {
                if(validateInput().isBlank()){
                    result = new Patient(Long.parseLong(socialSecurityNumberField.getText().trim()), firstNameField.getText().trim(),
                            lastNameField.getText().trim());
                    super.close();
                }else{
                    createWarningAlert(validateInput());
                }
            } else if (mode == Mode.VIEW) {
                if(validateInput().isBlank()){
                    existingPatient.setSocialSecurityNumber(Long.parseLong(socialSecurityNumberField.getText().trim()));
                    existingPatient.setFirstName(firstNameField.getText().trim());
                    existingPatient.setLastName(lastNameField.getText().trim());
                    super.close();
                }else{
                    createWarningAlert(validateInput());
                }
            }
        });

        //Closing the stage if the user presses "Cancel"
        cancelButton.setOnAction(actionEvent -> super.close());
    }

    /**
     * Method for validating the user input, and creating
     * a String containing the feedback for the user
     *
     * @return String with feedback on the input
     */
    private String validateInput(){
        String explanation = "";

        if (firstNameField.getText().isBlank()) {
            explanation += "Patient must have a first name.\n";
        }

        if (lastNameField.getText().isBlank()) {
            explanation += "Patient must have a last name.\n";
        }

        if (socialSecurityNumberField.getText().isBlank()) {
            explanation += "Patient must have a social security number.\n";
        }

        try {
            Long.parseLong(socialSecurityNumberField.getText().trim());
        } catch (Exception e) {
            explanation += "Social security number must be numbers";
        }

        return explanation;
    }

    /**
     * Method responsible for creating and displaying
     * an alert for invalid output containing feedback
     * for the user on what is wrong
     */
    private void createWarningAlert(String explanation){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Invalid input");
        alert.setHeaderText("Invalid input");
        alert.setContentText(explanation);
        alert.showAndWait();
    }
}


