package ntnu.idatt2001.joelmt;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.List;

/**
 * Class for the application
 */
public class PatientApplication extends Application {
    TableView<Patient> tableView;
    PatientRegister data = new PatientRegister();
    MainController mainController = new MainController(this);

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Start method for the application that runs when the application is started
     * It is responsible for creating the stage, scene, and all the content that is to
     * be displayed there.
     *
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Patient Register");
        //Initializing and setting up the TableView
        tableView = (TableView) GUIElementsFactory.getElement("TABLEVIEW");
        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.setPrefSize(600, 200);

        //Initializing and setting up the GridPane
        GridPane gridPane = (GridPane) GUIElementsFactory.getElement("GRIDPANE");
        gridPane.setVgap(10);
        gridPane.setHgap(10);

        //Creating and setting the content of the first column in the TableView
        TableColumn<Patient, String> column1 = new TableColumn<>("First name");
        column1.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        //Creating and setting the content of the second column in the TableView
        TableColumn<Patient, String> column2 = new TableColumn<>("Last name");
        column2.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        //Creating and setting the content of the third column in the TableView
        TableColumn<Patient, String>  column3 = new TableColumn<>("Social Security Number");
        column3.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        //Adding the column to the TableView
        tableView.getColumns().add(column1);
        tableView.getColumns().add(column2);
        tableView.getColumns().add(column3);

        //Creating the ToolBar which contains buttons for adding, editing and removing patients
        ToolBar toolBar = (ToolBar) GUIElementsFactory.getElement("TOOLBAR");

        //Creating and setting up the button for adding patients to the register
        Button addPatient = (Button) GUIElementsFactory.getElement("BUTTON");
        addPatient.setGraphic(new ImageView("add.png"));
        addPatient.setOnAction(actionEvent -> mainController.addPatient());

        //Creating and setting up the button for editing patients in the register
        Button editPatient = (Button) GUIElementsFactory.getElement("BUTTON");
        editPatient.setGraphic(new ImageView("edit.png"));
        editPatient.setOnAction(actionEvent -> mainController.editPatient());

        //Creating and setting up the button for removing patients from the register
        Button removePatient = (Button) GUIElementsFactory.getElement("BUTTON");
        removePatient.setGraphic(new ImageView("remove.png"));
        removePatient.setOnAction(actionEvent -> mainController.removePatient());

        //Adding the Buttons to the ToolBar
        toolBar.getItems().addAll(addPatient,editPatient, removePatient);

        gridPane.add(createMenus(), 1, 1);
        gridPane.add(toolBar, 1, 2);
        gridPane.add(tableView, 1, 3);
        Scene scene = new Scene(gridPane, 620, 400 );
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Method responsible for created the menubar
     * with different options
     *
     * @return a menubar
     */
    private MenuBar createMenus() {
        MenuBar menuBar = (MenuBar) GUIElementsFactory.getElement("MENUBAR");

        //Creating the different Menus in the MenuBar
        Menu menuFile = new Menu("File");
        Menu menuEdit = new Menu("Edit");
        Menu menuHelp = new Menu("Help");

        //Creating and setting up all the MenuItems in the Menus
        MenuItem importFile = new MenuItem("Import data from .CSV");
        importFile.setOnAction(actionEvent -> mainController.readCsvFile());
        MenuItem exportFile = new MenuItem("Export data");
        exportFile.setOnAction(actionEvent -> mainController.writeCsvFile());
        MenuItem addPatient = new MenuItem("Add new Patient");
        addPatient.setOnAction(actionEvent -> mainController.addPatient());
        MenuItem editPatient = new MenuItem("Edit selected Patient");
        editPatient.setOnAction(actionEvent -> mainController.editPatient());
        MenuItem removePatient = new MenuItem("Remove selected Patient");
        removePatient.setOnAction(actionEvent -> mainController.removePatient());
        MenuItem about = new MenuItem("About");
        about.setOnAction(actionEvent -> createInformationDialog());

        menuFile.getItems().addAll(importFile, exportFile);
        menuEdit.getItems().addAll(addPatient, editPatient, removePatient);
        menuHelp.getItems().add(about);

        menuBar.getMenus().addAll(menuFile, menuEdit, menuHelp);

        return menuBar;
    }

    /**
     * Getter for the data, which is
     * the PatientRegister containing all the
     * information of all the patients in the application
     *
     * @return a PatientRegister object
     */
    public PatientRegister getData() {
        return data;
    }

    /**
     * Getter for the TableView object
     *
     * @return a TableView object
     */
    public TableView<Patient> getTableView() {
        return tableView;
    }

    /**
     * Method responsible for creating the
     * "About" information dialog
     *
     */
    private void createInformationDialog() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText("Patient Register");
        alert.setContentText("This application was created by\nJoel Mattias Tømmerbakk\n30.04.2021");
        alert.showAndWait();
    }

    /**
     * Method responsible for updating
     * all the information displayed in
     * the TableView
     *
     */
    public void update(){
        List<Patient> dataList = data.getPatientRegister();
        ObservableList<Patient> dataObservableList = FXCollections.observableList(dataList);
        tableView.setItems(dataObservableList);
        tableView.refresh();
    }
}
