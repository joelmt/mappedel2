package ntnu.idatt2001.joelmt;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents a register of patients
 *
 * @author joelmt
 */
public class PatientRegister implements Serializable {
    private ArrayList<Patient> patientRegister;

    /**
     * Constructor responsible for creating an ArrayList
     * representing a register of patients
     *
     */
    public PatientRegister() {
        patientRegister = new ArrayList<>();
    }

    /**
     * Getter for the patient register
     *
     * @return An ArrayList containing all registered patients
     */
    public ArrayList<Patient> getPatientRegister() {
        return patientRegister;
    }

    /**
     * Method for adding a patient to the register
     *
     * @return true if the patient was added successfully, false
     *         if the register already contains the patient
     */
    public boolean addPatient(Patient patient){
        if(!patientRegister.contains(patient)){
            patientRegister.add(patient);
            return true;
        }
        return false;
    }

    /**
     * Method for removing a patient from the register
     *
     * @return true if the patient was removed successfully, false otherwise
     */
    public boolean removePatient(Patient patient){
        return patientRegister.remove(patient);
    }

    /**
     * Method that returns the info of all the patients
     * in the register as a string
     *
     * @return a string containing all the info of all the patients
     *         in the register
     */
    @Override
    public String toString() {
        return "PatientRegister{" + patientRegister + '}';
    }
}
