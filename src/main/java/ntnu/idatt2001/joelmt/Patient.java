package ntnu.idatt2001.joelmt;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents a Patient object
 *
 * @author joelmt
 */

public class Patient implements Serializable {
    private long socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor for creating a patient object.
     *
     * @param socialSecurityNumber the social security number of a patient
     * @param firstName The first name of the patient
     * @param lastName The last name of the patient
     * @param diagnosis The patients diagnosis
     * @param generalPractitioner The general practitioner responsible for the patient
     * @throws IllegalArgumentException if any of the inputs are invalid
     */
    public Patient(long socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner) {
        if(socialSecurityNumber == 0L){
            throw new IllegalArgumentException("A patient must have a social security number");
        }
        if (firstName.isBlank()) {
            throw new IllegalArgumentException("A patient must have a first name");
        }
        if (lastName.isBlank()) {
            throw new IllegalArgumentException("A patient must have a last name");
        }
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Constructor for creating a patient object.
     * This constructor only takes the social security number,
     * first name and last name.
     *
     * @param socialSecurityNumber the social security number of a patient
     * @param firstName The first name of the patient
     * @param lastName The last name of the patient
     * @throws IllegalArgumentException if any of the inputs are invalid
     */
    public Patient(long socialSecurityNumber, String firstName, String lastName) {
        if(socialSecurityNumber == 0L){
            throw new IllegalArgumentException("A patient must have a social security number");
        }
        if (firstName.isBlank()) {
            throw new IllegalArgumentException("A patient must have a first name");
        }
        if (lastName.isBlank()) {
            throw new IllegalArgumentException("A patient must have a last name");
        }
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Getter for the social security number
     *
     * @return the patients social security number
     */
    public long getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * Getter for the first name
     *
     * @return the patients first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Getter for the last name
     *
     * @return the patients last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Getter for the diagnosis of the patient
     *
     * @return the patients diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Getter for the name of the general practitioner responsible
     * for the patient
     *
     * @return the patients general practitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Setter for the social security number
     *
     * @param socialSecurityNumber the new social security number
     */
    public void setSocialSecurityNumber(long socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Setter for the patients first name
     *
     * @param firstName the new first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Setter for the patients last name
     *
     * @param lastName the new last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Setter for the patients diagnosis
     *
     * @param diagnosis the new diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Setter for the patients general practitioner
     *
     * @param generalPractitioner the new general practitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Method that returns the info of the patient
     * as a string
     *
     * @return a string containing all the info of a patient
     */
    @Override
    public String toString() {
        return "Patient{" +
                socialSecurityNumber +
                ", " + firstName +
                ", " + lastName +
                ", " + diagnosis +
                ", " + generalPractitioner +
                '}';
    }

    /**
     * Method for comparing two patients to see if they are equal
     * Patients are considered equal if their social security number
     * are equal
     *
     * @param o object to compare to
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return socialSecurityNumber == patient.socialSecurityNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }
}
