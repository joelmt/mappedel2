package ntnu.idatt2001.joelmt;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

/**
 * Class responsible for creating GUI Element objects
 * that does not need to take a parameter
 */
public class GUIElementsFactory {
    /**
     * Method for getting an object depending of the parameter
     *
     * @param objectType the type of object to be created and returned
     * @return the node object
     */
    public static Node getElement(String objectType) {
        if (objectType == null) {
            return null;
        }
        if (objectType.equalsIgnoreCase("BUTTON")) {
            return new Button();
        } else if (objectType.equalsIgnoreCase("GRIDPANE")) {
            return new GridPane();
        } else if (objectType.equalsIgnoreCase("TEXTFIELD")) {
            return new TextField();
        } else if (objectType.equalsIgnoreCase("TABLEVIEW")) {
            return new TableView();
        } else if (objectType.equalsIgnoreCase("TOOLBAR")) {
            return new ToolBar();
        } else if (objectType.equalsIgnoreCase("MENUBAR")) {
            return new MenuBar();
        } else if (objectType.equalsIgnoreCase("LABEL")) {
            return new Label();
        }
        return null;
    }
}
