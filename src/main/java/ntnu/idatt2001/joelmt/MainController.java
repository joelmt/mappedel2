package ntnu.idatt2001.joelmt;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.*;
import java.util.Optional;

/**
 * Class responsible for handling all the
 * action events in the main application
 */
public class MainController {
    private PatientApplication app;

    /**
     * Constructor for the MainController class
     *
     * @param app The main application (PatientApplication)
     */
    public MainController(PatientApplication app) {
        this.app = app;
    }

    /**
     * Method responsible for displaying and
     * getting the data from the PatientStage
     *  window, and adding the new Patient to the
     *  data
     */
    public void addPatient() {
        PatientStage patientStage = new PatientStage();
        patientStage.showAndWait();

        Patient result = patientStage.getResult();
        if (result != null) {
            if (!app.getData().addPatient(result)) {
                createAddPatientAlert();
            }
        }
        app.update();
    }

    /**
     * Method responsible for displaying and
     * getting the data from the PatientStage
     *  window, and updating the info of
     *  the selected patient
     */
    public void editPatient() {
        if (!(app.getTableView().getSelectionModel().getSelectedItem() == null)) {
            PatientStage patientStage = new PatientStage(app.getTableView().getSelectionModel().getSelectedItem());
            patientStage.showAndWait();
            app.update();
        } else {
            createPatientSelectionAlert();
        }
    }

    /**
     * Method responsible for removing
     * the selected patient
     */
    public void removePatient() {
        if (!(app.getTableView().getSelectionModel().getSelectedItem() == null)) {
            if (showDeleteConfirmationDialog()) {
                app.getData().removePatient(app.getTableView().getSelectionModel().getSelectedItem());
                app.update();
            }
        } else {
            createPatientSelectionAlert();
        }
    }

    /**
     * Method responsible creating and displaying
     * a FileChooser, and reading the data from
     * the selected file and updating
     * the displayed data in the TableView
     */
    public void readCsvFile() {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(new Stage());

        String fileExtension;
        String fileName;

        if (file != null) {
            fileName = file.toString();

            //Getting the extension of the file, by getting the substring after the last "." (including the ".")
            fileExtension = fileName.substring(fileName.lastIndexOf("."));

            //Checking the selected file has the corrected extension, and if not, displaying an alert and running the method again
            if (!fileExtension.equals(".csv")) {
                createFileTypeAlert();
                readCsvFile();
            }

            if (fileExtension.equals(".csv")) {
                BufferedReader fileReader = null;
                try {
                    PatientRegister patientRegister = new PatientRegister();
                    String line;

                    fileReader = new BufferedReader(new FileReader(fileName));
                    //Reading the first line of the file (the header) in order to skip it
                    fileReader.readLine();

                    //Reading the file line by line, but skipping the first line
                    while ((line = fileReader.readLine()) != null) {

                        //Creating a string array by splitting the contents of each line on the ";"
                        String[] tokens = line.split(";");

                        if (tokens.length > 0) {
                            //Creating new Patient objects using the data from the file and adding them to the patientRegister
                            Patient patient = new Patient(Long.parseLong(tokens[0]), tokens[1], tokens[2], tokens[3], tokens[4]);
                            patientRegister.addPatient(patient);
                        }
                    }
                    //Setting the PatientRegister in the application to the data from the file
                    app.data = patientRegister;
                } catch(Exception e) {
                    System.out.println("Error in CsvFileReader");
                } finally {
                    try {
                        fileReader.close();
                    } catch (IOException e) {
                        System.out.println("Error while closing fileReader.");
                    }
                }
                app.update();
            }
        }
    }

    /**
     * Method responsible creating and displaying
     * a FileChooser, and writing the data from the
     * application into a new file
     */
    public void writeCsvFile() {
        FileWriter fileWriter = null;
        String fileName;

        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(new Stage());

        if (file != null) {
            fileName = file.toString();

            try {
                fileWriter = new FileWriter(fileName);

                //Adding the header for the file, which shows what each column represents
                fileWriter.append("socialSecurityNumber;firstName;lastName;diagnosis;generalPractitioner");
                fileWriter.append("\n");

                //Adding all the information from to register to the fileWriter, with each column separated by a ";"
                for (Patient p : app.getData().getPatientRegister()) {
                    fileWriter.append(String.valueOf(p.getSocialSecurityNumber()));
                    fileWriter.append(";");

                    fileWriter.append(p.getFirstName());
                    fileWriter.append(";");

                    fileWriter.append(p.getLastName());
                    fileWriter.append(";");

                    fileWriter.append(p.getDiagnosis());
                    fileWriter.append(";");

                    fileWriter.append(p.getGeneralPractitioner());
                    fileWriter.append("\n");
                }
            } catch(Exception e) {
                System.out.println("Error in CsvFileWriter.");
            } finally {
                try {
                    fileWriter.flush();
                    fileWriter.close();
                }catch (IOException e) {
                    System.out.println("Error while flushing/closing fileWriter.");
                }
            }
        }

    }

    /**
     * Method responsible creating and showing
     * an alert for when the patient attempted to add
     * already exists in the register
     */
    private void createAddPatientAlert(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Patient already exists");
        alert.setHeaderText("Patient already exists");
        alert.setContentText("This patient already existing in the register.\nTry registering a different patient");
        alert.showAndWait();
    }

    /**
     * Method responsible creating and showing
     * an alert for when the user tries to
     * edit or remove a patient without
     * selecting one
     */
    private void createPatientSelectionAlert() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Selection");
        alert.setHeaderText("Select a patient");
        alert.setContentText("Please select a patient in order to edit or remove it.");
        alert.showAndWait();
    }

    /**
     * Method responsible creating and showing
     * a confirmation dialog when attempting to
     * remove a patient from the register
     */
    private boolean showDeleteConfirmationDialog() {
        boolean deleteConfirmed = false;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Delete confirmation");
        alert.setContentText("Are you sure you want to delete this task?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            deleteConfirmed = (result.get() == ButtonType.OK);
        }
        return deleteConfirmed;
    }

    /**
     * Method responsible creating and showing
     * an alert for when the selected file for import
     * has an invalid extension
     */
    private void createFileTypeAlert() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("File Type");
        alert.setHeaderText("Invalid file type");
        alert.setContentText("The chosen file type is invalid.\nPlease choose a file with the .csv file type.");
        alert.showAndWait();
    }
}
